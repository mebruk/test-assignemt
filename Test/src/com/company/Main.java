package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;


import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession");
        driver.findElement(By.id("identifierId")).sendKeys("birukab21@gmail.com");
        driver.findElement(By.id("identifierNext")).click();
        WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(password));
        password.sendKeys("@mariam21");
        driver.findElement(By.id("passwordNext")).click();
        wait.until(presenceOfElementLocated(By.xpath("//*[@aria-label='Primary']")));
        Thread.sleep(2000);
        List<WebElement> msg = driver.findElements(By.className("zE"));
        for (int i =0; i < msg.size(); i++){
            System.out.println("Sent By: " + msg.get(i).findElement(By.className("zF")).getAttribute("email"));
            System.out.println("Message: " + msg.get(i).findElement(By.className("y2")).getText());
        }
        String messg = "messages.txt";
        FileWriter writer = null;
        try {
            writer = new FileWriter(messg);
            for (int i =0; i < msg.size(); i++){
                writer.append("************************************************************************* \n");
                writer.append("Sent By: " + msg.get(i).findElement(By.className("zF")).getAttribute("email") + "\n");
                writer.append("Message: " + msg.get(i).findElement(By.className("bog")).getText() + "\n");
                writer.append("\n");
                writer.flush();
            }
        } catch (IOException ex) {

        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
            }
        }
        driver.quit();
    }
}
