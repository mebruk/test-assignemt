import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PortalTest {
    public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {
        System.setProperty("webdriver.gecko.driver", "C:/drivers/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        String appUrl = "https://portal.aait.edu.et";
        driver.get(appUrl);
        String title = driver.getTitle();
        System.out.println(title);
        driver.findElement(By.name("UserName")).sendKeys("ATR/xxxx/09");
        driver.findElement(By.id("Password")).sendKeys("xxxx");
        driver.findElement(By.className("btn-success")).click();
        String reportUrl = driver.findElement(By.id("m2")).findElement(By.className("dropdown-menu")).findElements(By.tagName("li")).get(0).findElements(By.tagName("a")).get(0).getAttribute("href");
        driver.navigate().to(reportUrl);
        String table = driver.findElement(By.className("table")).getText();
        System.out.println(table);
        PrintWriter writer = new PrintWriter("gradeReport", "UTF-8");
        writer.print(table);
        writer.close();
        Thread.sleep(5000);
        driver.quit();
        System.out.println("Finished");
    }
}

